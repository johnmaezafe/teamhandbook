import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:liquid_menu_bar/Pages/Home.dart';
import 'package:liquid_menu_bar/Pages/chart.dart';

void main() => runApp(MaterialApp(home: BottomNavBar()));

class BottomNavBar extends StatefulWidget {
  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  int pageIndex = 0;

  // Create All Pages

  final Home _home = Home();
  final Map _map = Map();
  final Chart _chart = Chart();
  GlobalKey _bottomNavigationKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: CurvedNavigationBar(
          key: _bottomNavigationKey,
          index: 0,
          height: 50.0,
          items: <Widget>[
            Icon(Icons.home, size: 30),
            Icon(Icons.list, size: 30),
            Icon(Icons.map, size: 30),
            Icon(Icons.people, size: 30),

          ],
          color: Colors.white,
          buttonBackgroundColor: Colors.white,
          backgroundColor: Colors.redAccent,
          animationCurve: Curves.easeInOut,
          animationDuration: Duration(milliseconds: 600),
          onTap: (index) {
            setState(() {
              pageIndex = index;
            });
          },
        ),
        body: Container(
          color: Colors.redAccent,
          child: Center(
            child: Column(
              children: <Widget>[
                Text(pageIndex.toString(), textScaleFactor: 10.0),

              ],
            ),
          ),
        ));
  }
}