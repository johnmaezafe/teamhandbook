<?php

if(isset($_GET['search']))
{
    $valueToSearch = $_GET['valueToSearch'];
    // search in all table columns
    // using concat mysql function
    $query = " SELECT * FROM `register` WHERE CONCAT(`studentnumber`, `lastname`, `firstname`, `middleinitial`, `program`, `uewebaddress`, `contactnumber`) LIKE '%".$valueToSearch."%'";
    $search_result = filterTable($query);
    
}
 else {
    $query = "SELECT * FROM `register`";
    $search_result = filterTable($query);
}

// function to connect and execute the query
function filterTable($query)
{
    $connect = mysqli_connect("localhost", "root", "", "registration");
    $filter_Result = mysqli_query($connect, $query);
    return $filter_Result;
}

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Search</title>
	</head>
	<body>

		<form action="SearchPage.php" method="POST">
			<input type="text" name="valueToSearch" placeholder="Search....">
			<input type="submit" name="search" value="Filter"><br><br>


<table width="70%" cellpadding="5" cellspacing="5">

	<tr>
		<td> StudentNumber </td>
		<td> LastName </td>
		<td> FirstName </td>
		<td> Middle Initial </td>
		<td> Program </td>
		<td> Web Address </td>
		<td> Contact Number </td>
	</tr>

	<?php while($row = mysqli_fetch_array($search_result)):?>
	<tr>
		<td><?php echo $row['studentnumber'];?> </td>
		<td><?php echo $row['lastname'];?> </td>
		<td><?php echo $row['firstname'];?> </td>
		<td><?php echo $row['middleinitial'];?> </td>
		<td><?php echo $row['program'];?> </td>
		<td><?php echo $row['uewebaddress'];?> </td>
		<td><?php echo $row['contactnumber'];?> </td>
	</tr>
<?php endwhile; ?>

	

</table>
</form>

<form action="index.php" method="POST">
	<input type="submit" name="back" value="HomePage" >

</form>
</body>
</html>