<?php

if(isset($_GET['search']))
{
    $valueToSearch = $_GET['valueToSearch'];
    // search in all table columns
    // using concat mysql function
    $query = " SELECT * FROM `register` WHERE CONCAT(`studentnumber`, `lastname`, `firstname`, `middleinitial`, `program`, `uewebaddress`, `contactnumber`) LIKE '%".$valueToSearch."%'";
    $search_result = filterTable($query);
    
}
 else {
    $query = "SELECT * FROM `register`";
    $search_result = filterTable($query);
}

// function to connect and execute the query
function filterTable($query)
{
    $connect = mysqli_connect("localhost", "root", "", "registration");
    $filter_Result = mysqli_query($connect, $query);
    return $filter_Result;
}

?>
<!DOCTYPE html>
<html>
	<head>

		<title>Search</title>
		<form action="index.php" method="GET">
			
			<nav class="navbar "style="background-color: #e3f2fd;">
  <a class="navbar-brand" href="index.php">Registration</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  
    </div>
</nav>
		
</form>
		 <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="index.css">
    <link rel="stylesheet" type="text/css" href="bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
	</head>
	<body>

		<form action="SearchPage.php" method="GET">

			<div class="p-3 mb-2 bg-info text-white">
			<input type="text" name="valueToSearch" placeholder="Search....">
			
			<button type="submit" class="btn btn-outline-dark" name="search" value="Filter">Filter</button>
			</div>

<table class="table table-striped table-dark" align="center">
		<thead>
	<tr>
		<th scope="col">StudentNumber</th>
		<th scope="col">Last Name</th>
		<th scope="col">First Name</th>
		<th scope="col">Middle Initial</th>
		<th scope="col">Program</th>
		<th scope="col">UE WEB ADDRESS</th>
		<th scope="col">Contact Number</th>
	</tr>
</thead>

	<?php while($row = mysqli_fetch_array($search_result)):?>
	<tr>
		<td><?php echo $row['studentnumber'];?> </td>
		<td><?php echo $row['lastname'];?> </td>
		<td><?php echo $row['firstname'];?> </td>
		<td><?php echo $row['middleinitial'];?> </td>
		<td><?php echo $row['program'];?> </td>
		<td><?php echo $row['uewebaddress'];?> </td>
		<td><?php echo $row['contactnumber'];?> </td>
	</tr>
<?php endwhile; ?>

	

</table>
</form>


</body>
</html>