<!DOCTYPE html>
<html>
<head>

	<title>PHP CRUDS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="index.css">
    <link rel="stylesheet" type="text/css" href="bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">

    <script src="https://kit.fontawesome.com/2c3bff0c45.js"></script>
     
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.1/css/all.css" integrity="sha384-wxqG4glGB3nlqX0bi23nmgwCSjWIW13BdLUEYC4VIMehfbcro/ATkyDsF/AbIOVe" crossorigin="anonymous">

     <link rel="stylesheet" href="css/index.css">





<!DOCTYPE html>
<html>
  <body  >

      <nav class="navbar "style="background-color: #e3f2fd;">
  <a class="navbar-brand" href="SearchPage.php">Search Here</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  
    </div>
</nav>
    <!-- [SEARCH FORM] -->
  



    

    <!-- [SEARCH RESULTS] -->
   
  

</head>
<body style="background-color: #e6eeff;">



<nav class="navbar navbar-light" style="background-color: #ccdcff; ">
  <a class="navbar-brand"><image src="ue.png" width = "40" height="40" 
    alt=""></a>

  <form class="form-inline" method="POST">
    
  
   
</div>


 </form>
</nav>
</nav>
  </form>
</nav>


</div>

<script type="text/javascript" src="bootstrap.min.js"></script>


<?php require_once 'process.php'; 



?>
  
 
<?php 

  if (isset($_SESSION['message'])):  
	?>
 
 <div class="alert alert-<?=$_SESSION['msg_type']?>" >

 	<?php
          echo $_SESSION['message'];
          unset($_SESSION['message']);
            
 	?>

 </div>

<?php endif ?>





 <?php
  $mysqli = new mysqli('localhost', 'root', '', 'registration') or die(mysqli_error($mysqli));
  $result = $mysqli->query("SELECT * FROM register") or die($mysqli->error);
  
  //db result check :) 
  //pre_r($result);
  //pre_r($result->fetch_assoc());
  //pre_r($result->fetch_assoc());
  
  ?>

<br/>
<br/>

<?php

function pre_r($array) {
	echo '<pre>';
	print_r($array);
	echo '</pre>';
}

?>




<div class = "row justify-content-center">

<form action = "process.php" method="POST" 

style="background: rgba(0,0,0,.7); border: 0px 
double black; 
padding: 5px; 
background-repeat: no-repeat;
background-origin: border-box; "> 




 
<br/>

<div class= "form-group">

     <label><b></b></label> 
	 <input type="text" class="form-control" value="<?php echo $studentnumber; ?>"  placeholder="Student Number" name="snumber" maxlength="11" pattern = ".{11}"required> <br/>
	
	 <label><b></b></label>
	 <input type="text" class="form-control" value="<?php echo $lastname; ?>" placeholder="Last Name" name="lname" required> <br/>

     <label><b></b></label>
	 <input type="text" class="form-control" value="<?php echo $firstname; ?>" placeholder="First Name" name="fname" required> <br/>
	 
	 <label><b></b></label>
	 <input type="text" class="form-control" value="<?php echo $middleinitial; ?>" placeholder="Middle Initial" name="minitial"> <br/>
	 

	 <input list="program" name = "program" value="<?php echo $program; ?>" class="form-control"  placeholder = "-Choose Program - " required>
	<datalist id ="program"> 
	<optgroup label = "Engineering">
	<option value = "Civil Engineering (CE)">
	<option value = "Computer Engineering (CpE)">
	<option value = "Electrical Engineering(EE)">
	<option value = "Electronics and Communication Engineering(ECE)">
	<option value = "Mechanical Engineering(ME)"
	
  </datalist>
	</optgroup>
     </select>
     	<br/> 
	 
	<label><b></b></label> 
	<input type="email" class="form-control" value="<?php echo $uewebaddress; ?>" placeholder="email (@ue.edu.ph)" name="email" pattern=".+@ue.edu.ph" required> <br/>
	
	<label><b></b></label>
	<span></span>

	<input type="text" name="cnumber" class="form-control" value="<?php echo $contactnumber; ?>"  maxlength="13"  pattern="^(09|\+639)\d{9}$" placeholder="Contact Number (ex.+639xxxxxxxxx)" required>
  
  <div class="form-group">
  <?php
  if ($update == true):
  ?>	
      <button type = "submit" class="btn btn-info" name="update"><i class="fas fa-sync-alt"></i> Update</button>

    
  <?php else: ?>

	<button type = "submit" class="btn btn-primary" name="save"><i class="far fa-save"></i> SAVE</button>

<?php endif; ?>


</div>
</div>
</form>

  <div class="row justify-content-right">
  <div class="container">
 <table class="table table-bordered table-dark" id = "mytable" >
  
 
  <thead>
    <tr>
      <th><center>Student Number</center></th>
      <th><center>Last Name</center></th>
      <th><center>First Name</center></th>
      <th><center>Middle Initial</center></th>
      <th><center>Program</center></th>
      <th><center>UE Web Address</center></th>
      <th><center>Contact Number</center></th>
      <th colspan="2"><center>Action</center></th>


    </tr>

  </thead>



<?php
    
  
     while ($row = $result->fetch_assoc()):
      ?>
      
     <tr>
      <td><?php echo $row['studentnumber']; ?> </td>
      <td><?php echo $row['lastname']; ?> </td>
      <td><?php echo $row['firstname']; ?> </td>
      <td><?php echo $row['middleinitial']; ?> </td>
      <td><?php echo $row['program']; ?> </td>
      <td><?php echo $row['uewebaddress']; ?> </td>
      <td><?php echo $row['contactnumber']; ?> </td>
        <td>
          <a href="index.php?edit=<?php echo $row ['studentnumber']; ?>"
            class="btn btn-info"><i class="far fa-edit"></i></a>

          <a href="process.php?delete=<?php echo $row['studentnumber']; ?>"
            class= "btn btn-danger"><i class="far fa-trash-alt"></i></a>

        </td>

   
     </tr>

 <?php endwhile; ?>



  
 </table>




</div>




<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>


<script >
/*	$(document).ready(function() {

    $('#datatablestudentnumber').DataTable({

    	"pagingType": "full_numbers",
    	"lengthMenu": [
    	[10, 25, 50, -1],
    	[10, 25, 50, "All"]


    	],
    	responsive: true,
    	language: {
    		search: "INPUT",
    		searchPlaceholder:" Search Records",

    	}
    });
} );   
*/

</script>

	
	


</form>


</div>
</div>


 <br/>
 
</form>
</body>
</html>

