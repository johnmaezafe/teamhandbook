import 'package:flutter/material.dart';

void main(){
  runApp(MaterialApp(
    title:  'Drawer App',
    home:  HomeScreen(),

  )); // Material App
}


class HomeScreen extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: Appbar(
        title: Text('Drawer Tutorial'),
        backgroundColor : Colors.deepOrange,
      ), // appbar
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(child: Text('this is Drawer Header'),),
            ListTile(
              title: Text('this is tile 1'),

            ),
            ListTile(
              title: Text('this is tile 2'),

            ),
            ListTile(
              title: Text('this is tile 3'),

            ),
            ListTile(
              title: Text('this is tile 4'),

            )
          ],
        ),
      ),
    );
  }



}
