import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: 'Drawer App',
    home: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('drawer tutorial'),
        backgroundColor: Colors.deepOrange,
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                gradient:  LinearGradient(colors: <Color>[
                  Colors.deepOrange,
                  Colors.blueAccent
                ])
              ),
              child: Container(
                child: Column(
                  children: <Widget>[
                    Material(
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      child: Padding(padding: EdgeInsets.all(2.0),
                         child: Image.asset('images/uehandbook.png',width: 100, height: 100,)) ,
                    ),
                    Padding(padding: EdgeInsets.all(3.0),
                    child:  Text('UE Electronic Handbook', style: TextStyle(color: Colors.white,fontSize: 19.0),),)
                  ],
                ),
              )),
            CustomListTile(Icons.person,'Profile',() => {}),
            CustomListTile(Icons.notifications, 'Notification', () => {}),
            CustomListTile(Icons.settings, 'Settings' , () => {}),
            CustomListTile(Icons.map, 'maps', () => {}),
            CustomListTile(Icons.cloud_queue, 'Clouds' , () => {})
          ],
        ),
      ),
    );
  }



}

class CustomListTile extends StatelessWidget{

  IconData icon;
  String text;
  Function onTap;

  CustomListTile(this.icon,this.text,this.onTap);


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
      child: Container(
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.black38))
        ),
        child: InkWell(
          splashColor: Colors.redAccent,
          onTap: onTap,
          child: Container(
            height: 55,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Icon(icon),
                    Text(text,style: TextStyle( fontSize: 15.0),),
                  ],
                ),
                Icon(Icons.keyboard_arrow_right),
              ],
            ),
          ),
        ),
      ),
    );
  }


}