import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:menu_bar_nextpage/Pages/charts.dart';
import 'package:menu_bar_nextpage/Pages/lists.dart';
import 'package:menu_bar_nextpage/Pages/maps.dart';

import 'Pages/home.dart';

void main() {
  runApp(MaterialApp(home: BottomNavBar()));
}


class BottomNavBar extends StatefulWidget {
  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  int _page = 1;

  //Create all the pages

  final Home _home = Home();
  final Lists _list = Lists();
  final Maps _map = Maps();
  final Charts _chart = Charts();

  Widget _showpage = new Home();
  Widget _pageChooser(int page){
    switch (page){
      case 0:
        return _list;
        break;
      case 1:
        return _home;
        break;
      case 2:
        return _map;
        break;
      case 3:
        return _chart;
        break;
      default:
        return new Container(
          child: new Center(
            child: new Text(
              'No Page Found', style: new TextStyle(fontSize: 30),
            ),
          ),
        );


    }
  }

  GlobalKey _bottomNavigationKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: Drawer(
          child: ListView(
            children: <Widget>[
              DrawerHeader(
                  decoration: BoxDecoration(
                      gradient:  LinearGradient(colors: <Color>[
                        Colors.deepOrange,
                        Colors.blueAccent
                ])
              ),
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Material(
                        borderRadius: BorderRadius.all(Radius.circular(20.0)),
                        child: Padding(padding: EdgeInsets.all(2.0),
                            child: Image.asset('assets/uehandbook.png',width: 100, height: 100,)) ,
                      ),
                      Padding(padding: EdgeInsets.all(3.0),
                        child:  Text('UE Electronic Handbook', style: TextStyle(color: Colors.white,fontSize: 19.0),),)
                    ],
                  ),
                )),
            CustomListTile(Icons.person,'Profile',() => {}),
            CustomListTile(Icons.notifications, 'Notification', () => {}),
            CustomListTile(Icons.settings, 'Settings' , () => {}),
            CustomListTile(Icons.map, 'maps', () => {}),
            CustomListTile(Icons.cloud_queue, 'Clouds' , () => {})
          ],
        ),
      ),
        bottomNavigationBar: CurvedNavigationBar(
          key: _bottomNavigationKey,
          index: _page,
          height: 50.0,
          items: <Widget>[
            Icon(Icons.list, size: 30),
            Icon(Icons.home, size: 30),

            Icon(Icons.map, size: 30),
            Icon(Icons.people, size: 30),
          ],
          color: Colors.white,
          buttonBackgroundColor: Colors.white,
          backgroundColor: Colors.blueAccent,
          animationCurve: Curves.easeInOut,
          animationDuration: Duration(milliseconds: 600),
          onTap: (int tappedIndex) {
            setState(() {
              _showpage = _pageChooser(tappedIndex);
            });
          },
        ),
        body: Container(
          color: Colors.blueAccent,
          child: Center(
            child: _showpage,

          ),
        ));
  }
}

class CustomListTile extends StatelessWidget{

  IconData icon;
  String text;
  Function onTap;

  CustomListTile(this.icon,this.text,this.onTap);


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
      child: Container(
        decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.black38))
        ),
        child: InkWell(
          splashColor: Colors.redAccent,
          onTap: onTap,
          child: Container(
            height: 55,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Icon(icon),
                    Text(text,style: TextStyle( fontSize: 15.0),),
                  ],
                ),
                Icon(Icons.keyboard_arrow_right),
              ],
            ),
          ),
        ),
      ),
    );
  }


}
