package suyatl.example.com.uehandbook;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

public class TopicFragment extends Fragment {

    List<Topics> lstbook;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_topics,container,false);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_topics);
        lstbook = new ArrayList<>();
        lstbook.add(new Topics("Student Grading", "Category, Topic" , "Topic Description", R.drawable.ic_menu_camera));
    }

    private void setContentView(int fragment_topics) {
    }
}
