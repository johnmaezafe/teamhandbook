package suyatl.example.com.staggeredtest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView staggeredRv;
    private  StaggeredRecyclerAdapter adapter;
    private StaggeredGridLayoutManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        //full screen UI


        getSupportActionBar().hide();

        staggeredRv = findViewById(R.id.staggered_rv);
        manager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        staggeredRv.setLayoutManager(manager);

        //lets create a simple array list of images


        List<row> lst = new ArrayList<>();

        lst.add(new row(R.drawable.boy1));
        lst.add(new row(R.drawable.boy2));
        lst.add(new row(R.drawable.vain));
        lst.add(new row(R.drawable.cute1));
        lst.add(new row(R.drawable.cute2));
        lst.add(new row(R.drawable.vain));
        lst.add(new row(R.drawable.girl1));
        lst.add(new row(R.drawable.girl2));
        lst.add(new row(R.drawable.girl3));
        lst.add(new row(R.drawable.girl4));
        lst.add(new row(R.drawable.girl5));
        lst.add(new row(R.drawable.vain));
        lst.add(new row(R.drawable.boy1));
        lst.add(new row(R.drawable.boy2));
        lst.add(new row(R.drawable.vain));
        lst.add(new row(R.drawable.cute1));
        lst.add(new row(R.drawable.cute2));
        lst.add(new row(R.drawable.vain));
        lst.add(new row(R.drawable.girl1));
        lst.add(new row(R.drawable.girl2));
        lst.add(new row(R.drawable.girl3));
        lst.add(new row(R.drawable.girl4));
        lst.add(new row(R.drawable.girl5));
        lst.add(new row(R.drawable.vain));



        adapter = new StaggeredRecyclerAdapter(this,lst);
        staggeredRv.setAdapter(adapter);

        

    }
}
